import React from 'react'
import styles from './Header.module.scss'

export default class Header extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className={styles['chat-header']}>
                <p>My Chat</p>
                <p>{this.props.participants} members</p>
                <p>{this.props.messages} messages</p>
                <p>Last message at: {this.props.lastMessage}</p>
            </div>
        )
    }
}