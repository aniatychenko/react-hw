import React from 'react'
import styles from './Chat.module.scss'
import Header from '../Header/Header'
import MessageList from '../MessageList/MessageList'
import MessageInput from '../MessageInput/MessageInput'
import Loading from '../Loading/Loading'
import EditMessageModal from '../EditMessageModal/EditMessageModal'

export default class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            textToEdit: '',
            idEditMessage: '',
            newMessage: {},
            allMessagesInfo: [],
            participentsAmount: 0,
            lastMessage: 0,
            messageAmount: 0,
            isLoading: true,
        };
    }
    currentUserId= "";
    currentUserName = "";
    lastMessageDate = 0;
    date = 0;
    hours = 0;
    minutes = 0;
    participants = new Set();
    participantsId = new Set();
    partIdArray = [];
    componentDidMount() {
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
        .then(result => result.json())
        .then(data => {
            data.sort(function(a,b){
                return new Date(a.createdAt) - new Date(b.createdAt);
            });
            this.setState({allMessagesInfo: data})
            this.state.allMessagesInfo.forEach(message => {
                this.date = new Date(message.createdAt);
                this.hours = this.date.getHours();
                this.minutes = this.date.getMinutes();
                this.lastMessage = this.hours + ":" + this.minutes;
                this.participants.add(message.user);
                this.participantsId.add(message.userId);
            })
            this.partIdArray = Array.from(this.participantsId);
            this.currentUserId = this.partIdArray[Math.floor(Math.random() * (this.partIdArray.length - 0))];
            this.currentUserName = this.state.allMessagesInfo.find(user => user.userId === this.currentUserId).user; 
            this.setState({ 
                participentsAmount: this.participants.size,
                lastMessage: this.lastMessage,
                messageAmount: this.state.allMessagesInfo.length,
                isLoading: false
            })
        })
      }
      sendButtonHandler = (text, id) => {
          if(!id) {
              const message = {
                  id: Math.random() * (1000),
                  userId: this.currentUserId,
                  text: text,
                  user: this.currentUserName,
                  createdAt: new Date(),
                  editedAt: ""
              }
    
            this.setState({
                newMessage: message,
                allMessagesInfo: [...this.state.allMessagesInfo, message]})
          }
          else{
            let arrayOfMessages = this.state.allMessagesInfo;
            arrayOfMessages.forEach(message => {
                if(message.id === id)
                {
                    message.text = text;
                    message.editedAt = new Date();
                }
            })
            this.setState({allMessagesInfo: arrayOfMessages, isCalledEdit: false});
          }
      }
      deleteIconHandler = (messageId) => {
        let arrayOfMessages = this.state.allMessagesInfo;
        let index = arrayOfMessages.findIndex(message => message.id === messageId);
        console.log(index);
        if (index !== -1) {
            arrayOfMessages.splice(index, 1);
            this.setState({allMessagesInfo: arrayOfMessages});
          }
    }
    editIconHandler = (text, id) => {
         this.setState({ isCalledEdit: true,
            textToEdit: text,
            idEditMessage: id
        })
    }

    render() {
        if(this.state.isLoading) {
            return <Loading />;
          }
        if(this.state.isCalledEdit) {
            return <EditMessageModal 
                editText={this.state.textToEdit}
                editId={this.state.idEditMessage}
                sendButtonHandler={this.sendButtonHandler}/>
        }
        return (
            <div className={styles['chat-wrp']}>
                <Header participants={this.state.participentsAmount}
                    messages={this.state.messageAmount}
                    lastMessage={this.state.lastMessage}/>
                <MessageList messageItemsInfo={this.state.allMessagesInfo}
                    currUser={this.currentUserId}
                    deleteIconHandler={this.deleteIconHandler}
                    editIconHandler={this.editIconHandler}/>
                <MessageInput sendButtonHandler={this.sendButtonHandler} />
            </div>
        )
    }
}