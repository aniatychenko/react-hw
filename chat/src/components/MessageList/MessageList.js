import React from 'react'
import styles from './MessageList.module.scss'
import MessageItem from '../MessageItem/MessageItem'
import DateComponent from '../DateComponent/DateComponent'
 

export default class MessageList extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
    }
    currDate = new Date();
    render() {
        return (
            <div className={styles['chat-list']}>
              {
                  Object.values(this.props.messageItemsInfo).map((val,i) =>{
                      let date = new Date(val.createdAt)
                      let time = date.getHours() + ':' + date.getMinutes()
                    if(this.currDate.getDate() !== date.getDate())
                    {
                        this.currDate = date;
                        return(
                            <div>
                                <DateComponent key={i+100} date={date}/>
                                <MessageItem
                                    key={i}
                                    deleteIconHandler={this.props.deleteIconHandler}
                                    editIconHandler={this.props.editIconHandler}
                                    messageId={val.id}
                                    name={val.user}
                                    text={val.text}
                                    avatar={val.avatar}
                                    userId={val.userId}
                                    time={time}
                                    editedAt={val.editedAt}
                                    currUser={this.props.currUser}/>
                            </div>
                        )
                    }
                  return(
                      <MessageItem
                        key={i+200} 
                        deleteIconHandler={this.props.deleteIconHandler}
                        editIconHandler={this.props.editIconHandler}
                        messageId={val.id}
                        name={val.user}
                        text={val.text}
                        avatar={val.avatar}
                        userId={val.userId}
                        time={time}
                        currUser={this.props.currUser}/>
                    )}
                )}
            </div>
        )
    }
}