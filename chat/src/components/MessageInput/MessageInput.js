import React from 'react'
import styles from './MessageInput.module.scss'
import sendIcon from '../../images/arrow-circle-up-solid.svg'

export default class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }
    handleChange = (ev) =>{
        this.setState({value: ev.target.value});
    }
    send = () => {
        this.setState({value: ''});
        this.props.sendButtonHandler(this.state.value)
    }
    render() {
        return (
            <div className={styles['message-input']}>
                <textarea
                    onChange={this.handleChange}
                    value={this.state.value}
                    placeholder="Message"
                    type="text"
                    className={styles['message-send-text']}></textarea>
                <button 
                    onClick={this.send}
                    id={styles['send-button']}>
                    <img  src={sendIcon} alt="send" width="32px"/>Send</button>
            </div>
        )
    }
}