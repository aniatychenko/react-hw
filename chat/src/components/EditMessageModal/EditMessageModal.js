import React from 'react'
import styles from './EditMessageModal.module.scss'

export default class EditMessageModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.editText
        }
    }
    handleChange = (ev) =>{
        this.setState({value: ev.target.value});
    }
    send = () => {
        this.setState({value: ''});
        this.props.sendButtonHandler(this.state.value, this.props.editId)
    }
    render() {
        return (
            <div className={styles['modal-block']}>
                <textarea 
                    onChange={this.handleChange} 
                    className={styles['edit-textarea']} 
                    value={this.state.value}> 
                </textarea>
                <button 
                    className={styles['edit-button']}
                    onClick={this.send}>Edit</button>
            </div>
        )
    }
}