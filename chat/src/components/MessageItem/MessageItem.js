import React from 'react'
import styles from './MessageItem.module.scss'
import like from '../../images/like.svg'
import edit from '../../images/edit.svg'
import deleteIcon from '../../images/delete.svg'

export default class MessageItem extends React.Component {
    constructor(props){
        super(props);
        this.state = {isLiked: false}
    }
    clickLikeHandler = (ev) => {
        this.setState(prevState => ({ isLiked: !prevState.isLiked }));
    }
    render() {

        const { isLiked } = this.state;
        if(this.props.currUser === this.props.userId){
            return (
                <div className={styles['your-message-item-wrp']}>
                    <div className={styles['icons-wrp']}>
                        <img src={edit}
                            alt="edit"
                            onClick={() => this.props.editIconHandler(this.props.text, this.props.messageId)}
                            className={styles['edit-icon']}/>
                        <img src={deleteIcon}
                            alt="delete"
                            onClick={() => this.props.deleteIconHandler(this.props.messageId)}
                            className={styles['delete-icon']}/>
                    </div>
                    <div className={styles['your-message-item']} you='true'>
                        <div className={styles['message-time']}>{this.props.time}</div>
                        <div className={styles['message-text']}>
                            <span className={styles['user-name']}>{this.props.name}</span>
                            <p className={styles['text']}>{this.props.text}</p>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className={styles['message-item-wrp']}>
                <div className={styles['message-item']}>
                    <img src={this.props.avatar} alt="ava" className={styles['message-image']}/>
                    <div className={styles['message-text']}>
                        <span className={styles['user-name']}>{this.props.name}</span>
                        <p className={styles['text']}>{this.props.text}</p>
                    </div>
                    <div className={styles['message-time']}>{this.props.time}</div>
                </div>
                <img src={like}
                    alt="like"
                    onClick={this.clickLikeHandler}
                    className={`${isLiked ? styles['like-icon-liked'] : styles['like-icon']}`}/>
            </div>
        )
    }
}