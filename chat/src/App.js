import './App.css';
import Chat from './components/Chat/Chat';
import logo from './images/logo.jpg';

function App() {
  return (
    <div className="App">
      <header><img src={logo} alt="Logo" width="150px"/></header>
      <Chat />
      <footer>© Powered by Binary Studio Team</footer>
    </div>
  );
}

export default App;
